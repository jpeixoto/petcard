import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import LoginContainer from './components/LoginContainer';

import store from './store';

import { Provider } from 'react-redux';


ReactDom.render((
	<Provider store={store}>
		<BrowserRouter>
			<LoginContainer />
		</BrowserRouter>
	</Provider>

	),
	document.getElementById("app")
);