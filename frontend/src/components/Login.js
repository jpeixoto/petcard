import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loginUser } from '../actions/authentication';

import classnames from 'classnames';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';


class Login extends React.Component{

	constructor(){
		super();
		this.state = {
			email: '',
			password: '',
			errors: {}
		}

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(e){
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	handleSubmit(e){
		e.preventDefault();
		const user = {
			email: this.state.email,
			password: this.state.password
		}
		this.props.loginUser(user);
	}

	componentDidMount() {
        if(this.props.auth.isAuthenticated) {
            this.props.history.push('/');
        }
    }

	componentWillReceiveProps(nextProps) {
		if(nextProps.auth.isAuthenticated) {
            this.props.history.push('/')
        }
        if(nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

	render(){
		const {errors} = this.state;
		return (
			<div className="formContainer">
				<Typography variant="h5">
            		Login
          		</Typography>
          		<form onSubmit={this.handleSubmit}>
					<TextField
						label="Email"
          				placeholder="Enter your Email"
          				type="email"
          				name="email"
          				className={classnames({
                        'is-invalid': errors.email
                    	})}
          				margin="normal"
          				onChange={this.handleInputChange}
          				value={this.state.email}
					/>
					{errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
					<br/>
					<TextField
						label="Password"
          				placeholder="Enter your Password"
						type="password"
						name="password"
						className={classnames({
                        'is-invalid': errors.password
                    	})} 
          				margin="normal"
          				onChange={this.handleInputChange}
          				value={this.state.password}
					/>
					{errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
					<br/>
					<Button type="submit" variant="contained" color="primary" className="btn">
        				Submit
      				</Button>
      				<Button component={Link} to="/register" color="secondary" className="btn">
  						Register
					</Button>
				</form>
	      	</div>
		)
	}
}

Login.propTypes = {
	loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	auth: state.auth,
    errors: state.errors
})

export default connect(mapStateToProps, { loginUser })(Login)