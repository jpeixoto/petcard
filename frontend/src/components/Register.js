import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { registerUser } from '../actions/authentication';

import classnames from 'classnames';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

class Register extends React.Component{							

	constructor(){
		super();
		this.state = {
			username: '',
			email: '',
			password: '',
			password_confirm:'',
			errors: {}
		}

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(e){
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	handleSubmit(e){
		e.preventDefault();
		const user = {
			username: this.state.username,
			email: this.state.email,
			password: this.state.password,
			password_confirm: this.state.password_confirm
		}

		this.props.registerUser(user, this.props.history);
	}

    componentWillReceiveProps(nextProps) {
    	if(nextProps.auth.isAuthenticated) {
            this.props.history.push('/')
        }
        if(nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    componentDidMount() {
        if(this.props.auth.isAuthenticated) {
            this.props.history.push('/');
        }
    }

	render(){
		const { errors } = this.state;
		return(
			<div className="formContainer">
				<Typography variant="h5">
            		Register
          		</Typography>
          		<form onSubmit={this.handleSubmit}>
					<TextField
						label="Username"
          				placeholder="Enter your Username"
          				type="text"
          				className={classnames({
                        'is-invalid': errors.username
                    	})}
          				name="username"
          				margin="normal"
						onChange={this.handleInputChange}
						value={this.state.username}
					/>
					{errors.username && (<div className="invalid-feedback">{errors.username}</div>)}
					<br/>
					<TextField
						label="Email"
          				placeholder="Enter your Email"
						type="email"
						className={classnames({
                        'is-invalid': errors.email
                    	})}
						name="email"
          				margin="normal"
						onChange={this.handleInputChange}
						value={this.state.email}
					/>
					{errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
					<br/>
					<TextField
						label="Password"
						placeholder="Enter your Password"
						type="password"
						className={classnames({
                        'is-invalid': errors.password
                    	})}
						name="password"
						margin="normal"
						onChange={this.handleInputChange}
						value={this.state.password}
					/>
					{errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
					<br/>
					<TextField
						label="Confirm Password"
						placeholder="Enter your Password again"
						type="password"
						className={classnames({
                        'is-invalid': errors.password_confirm
                    	})}
						name="password_confirm"
						margin="normal"
						onChange={this.handleInputChange}
						value={this.state.password_confirm}
					/>
					{errors.password_confirm && (<div className="invalid-feedback">{errors.password_confirm}</div>)}
					<br/>
					<Button type="submit" variant="contained" color="primary" className="btn">
        				Submit
      				</Button>
					<Button component={Link} to="/login" color="secondary" className="btn">
  						Cancel
					</Button>
				</form>
			</div>
		)
	}
}

Register.propTypes = {
    registerUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
	auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps,{ registerUser })(withRouter(Register))
