import React from 'react';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import { Provider, connect } from 'react-redux';
import store from '../store';
import PropTypes from 'prop-types';
import jwt_decode from 'jwt-decode';
import setAuthToken from '../setAuthToken';
import { setCurrentUser, logoutUser } from '../actions/authentication';

import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import PetsIcon from '@material-ui/icons/pets';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';

import {NotificationContainer} from 'react-notifications';

import Home from './Home';
import Login from './Login';
import Register from './Register';

import '../css/style.css';
import 'react-notifications/lib/notifications.css';


const theme = createMuiTheme({
  palette: {
    type: 'light',
  },
  typography: {
  	useNextVariants: true,
  },
});

if(localStorage.jwtToken) {
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decoded));

  const currentTime = Date.now() / 1000;
  if(decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = '/login'
  }
}

class LoginContainer extends React.Component{

	onLogout(e) {
        e.preventDefault();
        this.props.logoutUser(this.props.history);
    }

    componentDidMount(){
    	const {isAuthenticated} = this.props.auth;
        const { location, history } = this.props;
        const denyRedirect = ['/login', '/register'].filter(p => p === location.pathname)[0];

        if(!denyRedirect && !isAuthenticated){
            history.push('/login');
        }
    }

	render(){
		const {isAuthenticated, user} = this.props.auth;
        const authLinks = (
			<div>
				<div className="user-inf">
					<img src={user.avatar} alt={user.username} title={user.username}
						className="rounded-circle"
						style={{ width: '25px', marginRight: '5px'}} />
					<p>{user.username}</p>
					<a href="#" onClick={this.onLogout.bind(this)}>
						Logout
					</a>
				</div>
				<Switch>
					<Route exact path="/home" component={Home} />
					<Redirect to="/home" />
				</Switch>
			</div>
        )

        const guestLinks = (
			<Switch>
				<Route path='/login' component={Login}/>
				<Route path='/register' component={Register}/>
			</Switch>
      	)

		return (
			<Provider store = { store }>
				<div>
					<MuiThemeProvider theme={theme}>
						<AppBar position="static">
							<Toolbar>
								<PetsIcon className="petsIcon"/>
								<Typography variant="h5" color="inherit">
	            					Pet Card
	          					</Typography>
							</Toolbar>
						</AppBar>
						<div>
							{isAuthenticated ? authLinks : guestLinks}
						</div>
						<NotificationContainer/>
					</MuiThemeProvider>
				</div>
			</Provider>
		)
	}
}


LoginContainer.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth
})
export default withRouter(connect(mapStateToProps, { logoutUser })(LoginContainer))
