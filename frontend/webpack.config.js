var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require('path');

module.exports = {
  context: path.join(__dirname, "src"),
  devtool: debug ? "inline-sourcemap" : null,
  entry: "./index.js",
  
  module: {
    rules: [
      { test: /\.css$/, 
        loader: "style-loader!css-loader" 
      },
      {
        test: /\.(ttf|eot|svg|woff(2)?)(\S+)?$/,
        loader: 'file-loader?name=[name].[ext]'
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ["@babel/preset-env","@babel/preset-react"]
        }
      }
    ]
  },
  
  output: {
    path: __dirname + "/public/",
    filename: "app.min.js"
  },

  devServer: {
    proxy: {
    '/api': {
        target: 'http://localhost:5000',
            secure: false,
            changeOrigin: true,
      }
    }
  },

  plugins: debug ? [] : [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
  ],
};