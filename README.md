# For Frontend

## Go inside frontend folder
- cd frontend

## Run
- npm install 
- sudo npm install -g webpack
- npm run dev

# For Backend

## Go inside backend folder
- cd backend

## Run
- nodemon app


# Pet Card <!-- Registration page, login and logout -->

## Using React, Redux, Webpack, Material UI and MongoDB

